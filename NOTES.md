# Go Server

The initial idea behind this server is to provide a working alternative that implements the SSB protocol. The hope is to extract portions out into their own packages as we go along to provide support for alternative use cases.

## Misc

- The Protocol: https://ssbc.github.io/scuttlebutt-protocol-guide/